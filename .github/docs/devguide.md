## Welcome

This is the development guide for the Mumble Repository

Mumble is a huge mono-repo structured using [turborepo]() it also uses pnpm to store the workspace data in the local cache.

## Workspace Checklist

## Installations

- [ ] make sure Nodejs is install
  - [ ] run `node -v`
  - [ ] run `npm -v`
- [ ] install [pnpm](https://pnpm.io/)
- [ ] install [RustLang](https://forge.rust-lang.org/infra/other-installation-methods.html)
- [ ] install [GoLang](https://go.dev/)
- [ ] install [Python](https://www.python.org/)
- [ ] c++ compiler may be required.

## Next Steps

1. Check the issues out for a `good_for_beginners` tag ( Not Required unless beginner )

2. Every contribution requires the user to open up a new branch:

```bash
  git checkout -b branch name
```

This will create a new branch based off of main.

2. Install all depedencies

- This is super easy because of `pnpm`

```bash
  pnpm install
```

3. Run the application { Check it out before you work on it. }

- Run

```bash
  pnpm tauri dev
```

## Important

Check Trello

- The trello can be found in the main README it is linked twice; litterally can't miss it.

It is vital if you have a issue please create on on [Github]()

Or, if you don't feel comfortable with the formalness of Github, say something in the [Discord]()

## FAQ

If your asking yourself, why do I need to install all of these things?

- [Rust](https://www.rust-lang.org/learn/get-started) is used to run the [Tauriapp]() and uses rustc to compile.
- [Golang](https://go.dev/) is used for our backend microservices, and some of our database integrations that are not required to be low-level.
- [Python](https://www.python.org/) is used for Data-collection and Testing.
- [C++](https://cplusplus.com/) used for the lower level connection to the system.
