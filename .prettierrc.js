module.exports = {
    printWidth: 80,
    trailingComma: 'all',
    tabWidth: 2,
    semi: true,
    endOfLine: 'auto',
    singleQuote: true,
    jsxSingleQuote: true,
    importOrder: ['___', '__', '<THIRD_PARTY_MODULES>', '^[./]'],
    importOrderSortSpecifiers: true,
    plugins: [require.resolve('@trivago/prettier-plugin-sort-imports')],
};
