# Mumble

> "Any fool can write code that a computer can understand. Good programmers write code that humans can understand." – Martin Fowler

<img 
  src='/packages/ui/assets/MumbleLogo.png' style="display: flex; justify-content: center;" 
  alt='Mumble Streaming Logo'
/>

## Download Links

> -- Mumble is still in active development. --
> ![MAC]() | ![WINDOWS]() | ![LINUX]() | ![IOS]() | ![ANDRIOD]()

# What is Mumble?

Mumble is stremaing platform for Churches to post their content globally.

# What problems deos Mumble solve?

Mumble solves these given problems:

- Streaming can be hard to setup:
  - By using our node based graph system, you will never have any trouble.
- Devices can sometimes not connect to the application:
  - By using our auto-detect-device system, you shouldn't have this erorr.
- Recording do not save in the Videos/Movies Folder by default:
  - Mumble not only allows you to choose where to save the recording, but it also creates a directory that contais:
    - Video Recording `{ Video of the content recorded. }`
    - Audio Recording `{ If syncing didn't work we give you the audio file. }`

# Monorepo structure:

Apps:

- `mobile`: a [React Native](https://reactnative.dev/) app, { not made }.
- `www`: a [React](https://reactjs.org/) webapp to host the streaming service.
- `landing`: A [React](https://reactjs.org/) app using Vite SSR & Vite pages.

Core:

- `mcore`: Core is written in [Rust](https://www.rust-lang.org/) and only Rust

  - This Core handles a lot of the specific system integrations. And some of the Global microservices are written here too.

- `www/core`: core here is written in [Python](https://www.python.org/) using Flask.

The Python service is serving simple requests, everything from Chat to Login handles, while Rust is serving the more complicated requests like Strems, Authentication, Analytics.

Packages:

- `ui`: A [React](https://reactjs.org/) Shared components library. [ will probably be removed ]
- `interface`: The complete user interface in React (used by apps `desktop`, `web` and `landing`)
- `config`: `eslint` configurations (includes `eslint-config-next`, `eslint-config-prettier` and all `tsconfig.json` configs used in the throughout the monorepo.)

### License

Mumble is under the [GNU General public license](./LICENSE.md)

Check out the Wiki for more information about Mumble Contributions.
