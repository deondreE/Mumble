import Link from 'next/link';
import { GoMarkGithub } from 'react-icons/go';

export const NormalButton = ({ className }: { className: string }) => {
  return (
    <div className='flex h-12 w-auto flex-row rounded-lg border-[#0D9488] bg-[#0D9488] p-2 shadow-lg'>
      <GoMarkGithub className='mt-2 bg-[#A5F3FC]' color='A5F3FC' />
      <div className='p-0.5' />
      <button className='jost flex w-auto p-1 text-[#FEF9C3] hover:underline'>
        <Link href='https://github.com/Mumble-Tech/Mumble' target='_blank'>
          Star On Github
        </Link>
      </button>
    </div>
  );
};
