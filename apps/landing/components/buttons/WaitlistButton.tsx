export const WaitlistButton = ({ className }: { className: string }) => {
  return <button className={className}>Join the waitlist</button>;
};
