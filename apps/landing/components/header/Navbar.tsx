import Image from 'next/image';
import Link from 'next/link';
import logo from '../../public/logo.ico';

export const Navbar = ({ className }: { className: string }) => {
  return (
    <div className='relative top-0 flex p-4'>
      <Image src={logo} alt='working dev' className={className} />
      <div className='p-2' />
      <Link href='/' target='_self' className='p-2'>
        <span className='jost text-4xl font-light text-white'>Mumble</span>
      </Link>
    </div>
  );
};
