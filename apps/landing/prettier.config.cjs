/** @type {import("prettier").Config} */
module.exports = {
  singleQuote: true,
  semi: true,
  plugins: [require.resolve('prettier-plugin-tailwindcss')],
};
