import { type NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import { FaDiscord } from 'react-icons/fa';
import { FiTwitter } from 'react-icons/fi';
import { GoMarkGithub } from 'react-icons/go';
import { NormalButton } from '../../components/buttons/NormalButton';
import { WaitlistButton } from '../../components/buttons/WaitlistButton';
import { Navbar } from '../../components/header/Navbar';
import warning from '../../public/Warning.svg';
import logo from '../../public/logo.ico';
import image from '../../public/mumble-image.svg';

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Mumble</title>
        <meta name='description' content='Mumble landing page!' />
        <link rel='icon' href='/logo.ico' />
      </Head>
      <main className='bg-[#0284C7]'>
        <Navbar className='h-16 w-16 rounded-full border-black' />
        <div className='p-2' />
        <div className='flex justify-center text-6xl text-[#363636]'>
          <span className='lato'>A Streaming Service of the Future.</span>
        </div>
        <div className='p-4' />
        <div className='flex justify-center text-[1rem] italic text-[#D1FAE5] underline'>
          <span className='lato'>
            Mumble changes everything... Just try it for yourself.
          </span>
        </div>
        <div className='p-4' />
        <div className='flex flex-row justify-center'>
          <WaitlistButton className='jost h-12 w-32 rounded-sm border-[#0D9488] bg-[#0D9488] text-[#FEF9C3] shadow-lg hover:underline' />
          <div className='p-8' />
          <NormalButton className='' />
        </div>

        <div className='flex justify-center text-[1rem] italic text-[#FFEDD5]  underline'>
          <span>
            Coming soon on macOS, Windows and Linux.Shortly after to iOS &
            Android.
          </span>
        </div>
        <div className='p-4' />
        <div className='flex h-auto w-auto justify-center'>
          <div className='grid h-auto w-[80%] grid-cols-2 rounded-xl border-[#14B8A6] bg-[#14B8A6]'>
            <div className='h-auto w-full bg-white'>
              <Image src={image} alt='test' className='shadow-lg' />
            </div>
            <div className='relative left-12 flex h-auto w-auto flex-col justify-center align-middle'>
              <div className='flex h-[90%] w-[80%] flex-col justify-center rounded-md border-transparent bg-[#0D9488] text-center align-middle'>
                <span className='lato text-2xl italic text-orange-300 underline'>
                  Features of Mumble
                </span>
                <ol className='text-1xl jost italic text-white'>
                  <li>Simple format.</li>
                  <li>Multi transnational.</li>
                  <li>Find data easily.</li>
                  <li>Analytical Window.</li>
                  <li>Admin Dashboard.</li>
                  <li>Flow Graph interface.</li>
                  <li>Everything is free.</li>
                </ol>
              </div>
            </div>
            <div className='col-span-2 h-32 w-auto'>
              <div className='z-2 relative left-7 top-2 flex  h-28 w-[95%] flex-row bg-[#0891B2]'>
                <div className='p-4' />
                <div className='flex justify-center'>
                  <Image
                    src={warning}
                    alt='test image'
                    className='mt-2 h-24 w-24 drop-shadow-sm'
                  />
                  <div className='p-4' />
                  <span className='lato mt-8 w-[90%] p-2 text-left text-4xl italic text-[#FCA5A5]'>
                    ! Mumble will be in Open Beta august, 2023 !
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='p-12' />
        {/* Fotter */}
        <div className='h-auto w-full bg-[#0F766E]'>
          <div className='flex flex-col p-6'>
            <Image
              src={logo}
              alt='test'
              className='h-20 w-20 rounded-full border-transparent'
            />
            <div className='p-1' />
            <span className='text-2xl'>Mumble Streaming</span>
            <p className='text-[.5rem] italic text-emerald-200'>© Mumble.inc</p>
            <div className=''>
              <ul className='flex flex-row'>
                <li className='p-1'>
                  <Link
                    href='https://twitter.com/MumbleStreaming'
                    target='_blank'
                  >
                    <FiTwitter />
                  </Link>
                </li>
                <li className='p-1'>
                  <Link
                    href='https://github.com/Mumble-Tech/Mumble'
                    target='_blank'
                  >
                    <GoMarkGithub />
                  </Link>
                </li>
                <li className='p-1'>
                  <Link href='https://discord.gg/6D3CBWrvCE' target='_blank'>
                    <FaDiscord />
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default Home;
