from flask import Flask, redirect, flash, request
from flask_cors import CORS
from werkzeug.utils import secure_filename
from upload.util import allowed_file
from auth.login import Login
import os

# from .db import Client
# from .auth import Login

UPLOAD_FOLDER = "./files"


app = Flask(__name__)
app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
# Max lenght of the file in bytes
app.config["MAX_CONTENT_LENGTH"] = 16 * 1024 * 1024
CORS(app, origins="*")
# TODO: Connect to database


@app.route("/")
def home_route():
    return "This should be working home route"


# TODO: add all of the video routes
@app.route("/uploader", methods=["POST"])
def upload_file():
    if request.method == "POST":
        if "file" not in request.files:
            flash("No file part")
            return "No file part"
            # raise Exception("No file part")

    file = request.files["file"]
    if file.filename == "":
        flash("No selected file")
        return "test"

    # file is allowed according to the allowed_file function
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))
        return "File uploaded successfully"

    return ""


if __name__ == "__main__":
    app.run(debug=True, port="1337")
