import re


class Login:
    def __init__(self, username, password, email):
        self.username = username
        self.email = email
        self.password = password

    # def admin_login(self) -> bool:
    #     db = Client.create_client()
    #     # check database for admin user
    #     admin_user = db.user.find({"username": "admin"})
    #     if admin_user:
    #         pass
    #     else:
    #         raise Exception("Admin user not found")

    #     if self.username == admin_user.username and self.password == admin_user.password:
    #         return True
    #     else:
    #         raise Exception("Invalid username or password")

    def user_login(self) -> bool:
        # TODO: Username Validation : Filter for bad words, don't allow _/-)(#$(@(^@*&^$@)))
        email_validate_pattern = re.compile(r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$")
        if not email_validate_pattern.match(self.email):
            raise Exception("Invalid email")

        password_valid_pattern = re.compile(r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$")
        if not password_valid_pattern.match(self.password):
            raise Exception("Invalid password")

        # db = Client.create_client()

        # check database for user
        # check_user = db.user.find({"username": self.username})
        # if check_user:
        #     raise Exception("User already exists")
        # else:
        #     db.user.inser_one({"username": self.username, "password": self.password, "email": self.email})
        #     return True
