import os


class Validation:
    def __init__(self, filePath, mimetype):
        self.filePath = filePath
        self.mimetype = mimetype

    def validate(self):
        size = os.path.getSize(self.filePath)
        if size > 50 * 1024 * 1024:
            return False
        return True
