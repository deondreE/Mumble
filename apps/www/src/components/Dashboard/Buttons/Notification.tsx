import Image from 'next/image';
import bell from '../../../../public/icons/bell.svg';

export function NotificationBtn() {
  return (
    <div className='fixed right-12 top-8'>
      <button className='h-12 w-12 rounded-md border border-[#C685DD]'>
        <Image
          src={bell}
          alt='Notification Button'
          width={50}
          height={35}
          className='p-2'
        />
      </button>
    </div>
  );
}
