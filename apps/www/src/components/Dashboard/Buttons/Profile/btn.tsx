import { Text } from '@/components/Text/Text';

export function ProfileBtn() {
  return (
    <button className='text-.5xl ml-10 mt-4 h-12 w-20 rounded-md border text-white hover:border-[#C685DD]'>
      <Text innerText='edit' className='p-2' />
    </button>
  );
}
