import { Text } from '@/components/Text/Text';
import axios from 'axios';
import { useState } from 'react';
import styles from './upload.module.scss';

export function UploadForm() {
  const [file, setFile] = useState<any>(null);

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    let url = 'http://localhost:1337/uploader';
    const formData = new FormData();
    console.log(file);
    formData.append('file', file);

    axios
      .post(
        url,
        { file },
        { headers: { 'Content-Type': 'multipart/form-data' } },
      )
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  };

  return (
    <form className='h-auto w-auto p-4' encType='multipart/form'>
      <input
        type='file'
        className={styles.test}
        onChange={(e: any) => {
          setFile(e.target.files[0]);
        }}
      />
      <div className='p-1' />
      <button
        onClick={(e) => {
          handleSubmit(e);
        }}
        className='text-1xl rounded-md border p-2 text-white hover:border-[#C685DD]'
      >
        <Text innerText='Upload' className='text-1xl' />
      </button>
    </form>
  );
}
