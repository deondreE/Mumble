import { Text } from '@/components/Text/Text';
import { ProfileImage } from '@/components/assets/ProfileImage';
import { ProfileBtn } from '../Buttons/Profile/btn';

export function Profile({ ...props }: { username: string }) {
  return (
    <div className='mb-8 flex h-auto w-auto flex-col'>
      <div className='ml-8'>
        {/* Todo: Change Image */}
        <ProfileImage width={100} height={50} />
      </div>
      <div className='p-2' />
      <div className='justify-center align-middle text-white'>
        <Text innerText={props.username} className='text-1xl w-auto' />
      </div>
      <div>
        <ProfileBtn />
      </div>
    </div>
  );
}
