import { NavItem } from './nav-item/NavItem';

function Spacer() {
  return <div className='p-2' />;
}

export function Nav() {
  return (
    <div className='flex flex-col'>
      <NavItem innerText='Dashboard' />
      <Spacer />
      <NavItem innerText='Activty' />
      <Spacer />
      <NavItem innerText='Schedule' />
      <Spacer />
      <NavItem innerText='Settings' />
    </div>
  );
}
