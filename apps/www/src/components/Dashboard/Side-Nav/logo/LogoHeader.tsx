import { Text } from '@/components/Text/Text';
import { LogoNavBar } from '@/components/assets/Logo';

export function LogoHeader() {
  return (
    <div className='flex'>
      <div>
        <LogoNavBar />
      </div>
      <div className='p-4'>
        <Text innerText='Mumble' className='text-2xl text-white' />
      </div>
    </div>
  );
}
