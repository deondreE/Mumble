import { Text } from '@/components/Text/Text';

export function NavItem({ ...props }: { innerText: string }) {
  return (
    <div className='w-40 rounded-md border border-[#C685DD] p-2 text-white'>
      {/* {props.innerText} */}
      <Text
        innerText={props.innerText}
        className='aling-middle flex justify-center'
      />
    </div>
  );
}
