import { Follower } from '../assets/icons/SideNav/Follower';
import { Home } from '../assets/icons/SideNav/Home';
import { Payment } from '../assets/icons/SideNav/Payment';
import { Settings } from '../assets/icons/SideNav/Settings';
import { Share } from '../assets/icons/SideNav/Share';

export function SideNav() {
  return (
    <div className='flex-col'>
      <Home />
      <Share />
      <Payment />
      <Follower />
      <Settings />
    </div>
  );
}
