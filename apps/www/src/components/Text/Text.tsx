import styles from './text.module.css';

type TextProps = {
  innerText: string;
  className?: string;
};

function Heading({ ...props }: TextProps) {
  return (
    <h1 className={`${styles.ft} ${props.className}`}>{props.innerText}</h1>
  );
}

function HeadingLato({ ...props }: TextProps) {
  return (
    <h1 className={`${styles.ftGeneral} ${props.className}`}>
      {props.innerText}
    </h1>
  );
}

function Subheading({ ...props }: TextProps) {
  return <h3 className={props.className}>{props.innerText}</h3>;
}

function Text({ ...props }: TextProps) {
  return (
    <span className={`${styles.ft} ${props.className}`}>{props.innerText}</span>
  );
}

function TextLato({ ...props }: TextProps) {
  return (
    <span className={`${styles.ftGeneral} ${props.className}`}>
      {props.innerText}
    </span>
  );
}

export { Heading, HeadingLato, Subheading, Text, TextLato };
