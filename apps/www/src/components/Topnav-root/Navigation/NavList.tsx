function NavListItem({ ...props }: { text: string }) {
  return (
    <div className='p-4 text-white'>
      <span>{props.text}</span>
    </div>
  );
}

export function NavList() {
  return (
    <div className='flex justify-center p-4 align-middle'>
      <NavListItem text='Followed' />
      <NavListItem text='Top Live' />
      <NavListItem text='Churches' />
    </div>
  );
}
