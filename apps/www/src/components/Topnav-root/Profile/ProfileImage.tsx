export function ProfileImage() {
  return (
    <div className='flex justify-center p-2 align-middle'>
      <span className='h-16 w-16 cursor-pointer rounded-lg bg-[#D9D9D920] hover:border-[#C685DD]'></span>
    </div>
  );
}
