export function ProfileName({ ...props }: { name: string }) {
  return (
    <div className='flex justify-center p-7 align-middle'>
      <span className='text-white'>{props.name}</span>
    </div>
  );
}
