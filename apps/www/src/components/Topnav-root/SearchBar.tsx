export function SearchBar() {
  return (
    <div className='p-6'>
      <input
        type='text'
        placeholder='Search'
        className='flex w-96 justify-center rounded-md border-transparent bg-[#D9D9D920] p-2 align-middle placeholder:text-stone-300 hover:border-[rgb(198,133,221)]'
      />
    </div>
  );
}
