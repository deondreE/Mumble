import { LogoNavBar } from '../assets/Logo';
import { Messages } from '../assets/icons/Messages';
import { Notification } from '../assets/icons/Notification';
import { NavList } from './Navigation/NavList';
import { ProfileImage } from './Profile/ProfileImage';
import { ProfileName } from './Profile/ProfileName';
import { SearchBar } from './SearchBar';

export function TopNav() {
  return (
    <div className='flex p-6'>
      <div>
        <LogoNavBar />
      </div>
      <div className='p-4' />
      <div>
        <NavList />
      </div>
      <div className='p-12' />
      <div>
        <SearchBar />
      </div>
      <div className='p-12' />
      <div className='justify-bg-center mb-4 flex h-auto w-auto align-middle'>
        <Messages />
        <div className='p-4' />
        <Notification />
      </div>
      <div className='flex'>
        <ProfileName name='Deondre' />
        <ProfileImage />
      </div>
    </div>
  );
}
