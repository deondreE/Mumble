function Live() {
  return (
    <div className='h-8 w-20 rounded-md bg-[#D6778E] shadow-lg'>
      <span className='text-1xl flex justify-center p-1 text-center align-middle text-white'>
        live
      </span>
    </div>
  );
}

export function Stream({ ...props }: { isLive: boolean }) {
  return (
    <div className='p-2'>
      <div className='absolute p-4'>{props.isLive ? <Live /> : <></>}</div>
      <video className='w-90 h-72 rounded-md bg-red-200'></video>
    </div>
  );
}
