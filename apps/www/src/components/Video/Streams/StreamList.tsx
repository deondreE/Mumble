import { Stream } from './Stream';

export function StreamList() {
  return (
    <>
      <span className='p-2 text-2xl text-white'>Streams of the day</span>
      <div className='flex justify-center align-middle'>
        <Stream isLive={true} />
        <Stream isLive={false} />
        <Stream isLive={true} />
        <Stream isLive={false} />
      </div>
    </>
  );
}
