import { Heading } from '@/components/Text/Text';
import Image from 'next/image';
import image from './images/background-video.svg';
import dots from './images/icons/Dots.svg';
import skip from './images/icons/Skip.svg';
import fullscreen from './images/icons/fullscreen.svg';
import pause from './images/icons/pause.svg';
import volume from './images/icons/volume.svg';

export function ShowOff() {
  return (
    <div className='p-4'>
      <Image src={image} alt='video-image' className='z-10 h-auto w-auto' />
      <div className='z-20 flex flex-col'>
        <Heading
          innerText='broadcast starts in'
          className='align-center absolute bottom-64 ml-28 justify-center text-center text-white lg:text-4xl'
        />
        <Heading
          innerText='4:02'
          className='align-center absolute bottom-52 ml-60 justify-center text-center text-white sm:text-2xl md:text-3xl lg:text-4xl'
        />
      </div>
      <hr className='relative bottom-12 z-20 ml-6 h-2 w-[90%] border-stone-400' />
      <div className='relative bottom-12 z-20 flex w-full flex-row'>
        <Image src={pause} alt='pause-image' className='ml-6' />
        <Image src={skip} alt='pause-image' className='ml-1' />
        <Image src={volume} alt='pause-image' className='ml-1' />
        <div className='ml-4 h-8 rounded-md bg-[#D6778E] p-1'>
          <span className='text-1xl p-1 text-center text-white'>
            Coming Soon
          </span>
        </div>
        <div className='ml-2 p-1'>
          <span className='text-1xl text-white'>00:00</span>
        </div>
        <div className='ml-32' />
        <Image src={dots} alt='pause-image' className='ml-1' />
        <Image src={fullscreen} alt='pause-image' className='ml-1' />
      </div>
    </div>
  );
}
