import Image from 'next/image';
import logo from '../../../public/logo.svg';

export function LogoNavBar() {
  return <Image src={logo} alt='testing' />;
}
