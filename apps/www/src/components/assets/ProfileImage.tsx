import Image from 'next/image';
import image from '../../../public/profile-image.svg';

type ProfileImageProps = {
  width: number;
  height: number;
};

export function ProfileImage({ ...props }: ProfileImageProps) {
  return (
    <Image
      src={image}
      width={props.width}
      height={props.height}
      alt='Profile Image'
    />
  );
}
