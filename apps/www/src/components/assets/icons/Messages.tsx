import Image from 'next/image';
import message from '../../../../public/icons/message-circle.svg';

export function Messages() {
  return <Image src={message} alt='message center' />;
}
