import Image from 'next/image';
import notification from '../../../../public/icons/Bell.svg';

export function Notification() {
  return <Image src={notification} alt='notification center' />;
}
