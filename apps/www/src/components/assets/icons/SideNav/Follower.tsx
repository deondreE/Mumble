import Image from 'next/image';
import following from '../../../../../public/icons/SideNav/Following.svg';

export function Follower() {
  return <Image src={following} alt='follower-image' className='p-1' />;
}
