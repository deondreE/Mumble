import Image from 'next/image';
import home from '../../../../../public/icons/SideNav/home.svg';

export function Home() {
  return <Image src={home} alt='home-image' />;
}
