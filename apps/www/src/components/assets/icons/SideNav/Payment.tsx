import Image from 'next/image';
import payment from '../../../../../public/icons/SideNav/Shopping Card.svg';

export function Payment() {
  return <Image src={payment} alt='payment-image' className='p-1' />;
}
