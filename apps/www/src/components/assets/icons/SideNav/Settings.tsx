import Image from 'next/image';
import settings from '../../../../../public/icons/SideNav/setting.svg';

export function Settings() {
  return <Image src={settings} alt='settings-image' className='p-1' />;
}
