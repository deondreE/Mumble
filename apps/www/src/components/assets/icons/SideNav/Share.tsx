import Image from 'next/image';
import share from '../../../../../public/icons/SideNav/Upload.svg';

export function Share() {
  return <Image src={share} alt='share-image' className='p-1' />;
}
