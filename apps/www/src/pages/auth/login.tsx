import { SignInButton } from '@clerk/nextjs';

export default function Login() {
  return (
    <div>
      <SignInButton />
    </div>
  );
}
