import { NotificationBtn } from '@/components/Dashboard/Buttons/Notification';
import { UploadForm } from '@/components/Dashboard/Buttons/Upload/UploadForm';
import { Profile } from '@/components/Dashboard/Profile-dashboard/Profile';
import { Nav } from '@/components/Dashboard/Side-Nav/Nav';
import { LogoHeader } from '@/components/Dashboard/Side-Nav/logo/LogoHeader';
import { Heading } from '@/components/Text/Text';
import Head from 'next/head';
import { useRouter } from 'next/router';

export default function Dashboard() {
  // TODO: Get the logged in user
  const { id } = useRouter().query;
  return (
    <>
      <Head>
        <title>Mumble</title>
        <meta name='description' content='Mumble streaming app' />
        <meta name='viewport' content='width=device-width, initial-scale=1' />
        <link rel='icon' href='/logo.svg' />
      </Head>
      <main className='flex flex-row'>
        <div className='fixed left-64 flex'>
          <Heading innerText='Statistics' className='p-8 text-6xl text-white' />
          <NotificationBtn />
          <div className='fixed top-32 grid h-auto w-[87%] grid-cols-3 gap-10 p-6'>
            <div className='h-72 w-72 rounded-md border'>
              <UploadForm />
            </div>
            <div className='h-72 w-72 rounded-md border' />
            <div className='h-72 w-72 rounded-md border' />
            <div className='h-72 w-72 rounded-md border' />
            <div className='h-72 w-72 rounded-md border' />
            <div className='h-72 w-72 rounded-md border' />
          </div>
        </div>
        {/* <hr className='' /> */}
        <div className='h-full w-72 p-12 text-2xl'>
          <LogoHeader />
          <div className='p-2' />
          <Profile username='Deondre English' />
          <Nav />
        </div>
        {/* <h1 className='text-white'>Dashboard {id}</h1> */}
      </main>
    </>
  );
}
