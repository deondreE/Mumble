import { SideNav } from '@/components/Sidenav-root/SideNav';
import { Heading } from '@/components/Text/Text';
import { TopNav } from '@/components/Topnav-root/TopNav';
import { ShowOff } from '@/components/Video/Template/ShowOff';
import Head from 'next/head';

export default function Home() {
  return (
    <>
      <Head>
        <title>Mumble</title>
        <meta name='description' content='Mumble streaming app' />
        <meta name='viewport' content='width=device-width, initial-scale=1' />
        <link rel='icon' href='/logo.svg' />
      </Head>
      <main className='h-auto w-auto'>
        <TopNav />
        <div className='absolute left-32 flex h-auto w-[87%] rounded-lg border border-stone-600'>
          {/* Main Content of the page */}
          <div>
            <Heading
              innerText='WATCH, SHARE, FOLLOW POPULAR CHURCHES'
              className='p-4 text-5xl text-white sm:w-[6em] md:w-[8em] lg:w-[10em]'
            />
          </div>
          <div className='p-24' />
          <div>
            <ShowOff />
          </div>
        </div>
        <div className='absolute left-8 w-[20%] p-2'>
          <SideNav />
        </div>
      </main>
    </>
  );
}
