import axios from 'axios';
import { useState } from 'react';

export default function Upload() {
  const [file, setFile] = useState<any>(null);

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    let url = 'http://localhost:1337/uploader';
    const formData = new FormData();
    console.log(file);
    formData.append('file', file);

    axios
      .post(
        url,
        { file },
        { headers: { 'Content-Type': 'multipart/form-data' } },
      )
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  };

  return (
    <form encType='multipart/form'>
      <label>Upload File</label>
      <input
        onChange={(e: any) => {
          setFile(e.target.files[0]);
        }}
        type='file'
        className='text-white'
      />
      <button
        onClick={(e) => {
          handleSubmit(e);
        }}
        type='button'
        className='text-red-200'
      >
        Submit
      </button>
    </form>
  );
}
