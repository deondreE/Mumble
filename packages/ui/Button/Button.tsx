import React from 'react';

import { SkeletonButtonTypes } from './button.types';

export const SkeletonButton = ({className, children, onClick}: SkeletonButtonTypes) => {
  return (
    <button
      className={`${className}`}
      onClick={onClick}
    >
      { children }
    </button>
  );
};
