export interface SkeletonButtonTypes {
  className: string;
  children: React.ReactNode;
  onClick?: any;
};
