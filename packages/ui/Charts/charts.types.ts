interface GeneralChartTypes {
  width: number;
  height: number;
  data: Array<Object>;
};

export interface ScatterTypes extends GeneralChartTypes {
  strokeColor: string;
};
