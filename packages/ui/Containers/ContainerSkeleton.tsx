import React from 'react';

import {
  ContainerSkeletonTypes,
  MessageContianerTypes,
  ChatContainerTypes
} from './container.types';

export const ContainerSkeleton = ({className, children, width, height}: ContainerSkeletonTypes) => {
  return (
    <div
      className={`${className}`}
      style={{
        maxWidth: width,
        maxHeight: height,
        width: width,
        height: height,
      }}
    >
      {children}
    </div>
  );
};

export const ChatContainer = ({ children, width, height, className, id, color, font }: ChatContainerTypes) => {
  return (
    <div
      className={`${className}`}
      id={id}
      style={{
        maxWidth: width,
        maxHeight: height,
        height: height,
        width: width,
        color: color,
        fontFamily: font,
      }}
    >
      {children}
    </div>
  )
}

export const Message = ({
  width,
  height,
  message,
  username,
  messageTimeStyles,
  messageSpanStyles,
  messageStyles,
  className
}: MessageContianerTypes) => {
  return (
    <div
      style={{
        width: width,
        height: height,
        maxWidth: width,
        maxHeight: height
      }}
      className={`${className}`}
    >
      <span className={`${messageSpanStyles}`}>@{username} <span className={`${messageTimeStyles}`}>08:49 am</span></span>
      <span className={`${messageStyles}`}>{message}</span>
    </div>
  );
};
