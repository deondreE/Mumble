import React from "react";

export interface ContainerSkeletonTypes {
  className: string;
  children: React.ReactNode;
  width: string;
  height: string;
  id?: string;
};

export interface ChatContainerTypes extends ContainerSkeletonTypes {
  font?: string;
  color?: string;
}

export interface MessageContianerTypes {
  className: string;
  children?: React.ReactNode;
  width: string;
  height: string;
  message: string;
  username?: string;
  messageSpanStyles?: string;
  messageTimeStyles?: string;
  messageStyles?: string;
}
