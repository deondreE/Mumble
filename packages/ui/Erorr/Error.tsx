
import React from "react";
import { ErorrMessageTypes } from './error.types';

export const ErrorMessageSkeleton = ({message, className}: ErorrMessageTypes ) => {
  return (
    <div className={`border border-red-300 rounded-md w-auto h-auto p-4 flex justify-center text-2xl bg-red-300 shadow-lg ft-istok ${className}`}>
      <span className='w-auto h-auto'>{message}</span>
    </div>
  );
};

export const WarningMessageSkeleton = ({ message, className }: ErorrMessageTypes) => {
  return (
    <div className={`border border-yellow-400 ft-istok text-2xl w-auto h-auto p-4 rounded-lg bg-yellow-400 shadow-lg ${className}`}>
      <span className='w-auto h-auto'>{message}</span>
    </div>
  );
};
