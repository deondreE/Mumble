import React from 'react';
import { Tooltip } from 'react-tooltip';
import 'react-tooltip/dist/react-tooltip.css';

export const TooltipHover = ({ innerText }: { innerText: string }) => {
  return <Tooltip id="tooltip" place="top" content={innerText} />;
};
