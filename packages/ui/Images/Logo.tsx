import React from "react";

import MumbleLogo from '../assets/MumbleLogo.png';

export const Logo = () => {
  return (
    <img src={ MumbleLogo }  alt='' className=''/>
  )
};
