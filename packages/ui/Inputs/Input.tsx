import React from "react";
import { MessageInputTypes } from "./inputs.types";

export function MessageInput({ className, inputType, placeholder, onChange, onKeyPress }: MessageInputTypes) {
  return (
    <input
      className={`${className}`}
      type={inputType}
      placeholder={placeholder}
      onChange={onChange}
      onKeyDown={onKeyPress}
    />
  );
};
