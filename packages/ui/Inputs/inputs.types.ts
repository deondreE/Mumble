
export interface MessageInputTypes {
  className: string;
  inputType?: string | 'text';
  placeholder?: 'type message...' | string;
  onChange?: any;
  onKeyPress?: any;
};
