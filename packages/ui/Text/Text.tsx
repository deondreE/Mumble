import React from 'react';

interface TextPropsTypes {
  text?: string;
  className: string;
}

interface AnalyticsDateTime extends TextPropsTypes {
  hour?: number;
  minute?: number;
  second?: number;
}

export const HeadingText = ({ text, className }: TextPropsTypes) => {
  return <span className={`text-3xl ${className}`}>{text}</span>;
};

export const AnalyticsNumber = ({ className, text }: TextPropsTypes) => {
  return <span className={`text-3xl ${className}`}>{text}</span>;
};

export const AnalyticsTextGeneral = ({ className, text }: TextPropsTypes) => {
  return <span className={`text-1xl font-mono ${className}`}>{text}</span>;
};

export const AnalyticsDateTime = ({
  className,
  hour,
  minute,
  second,
}: AnalyticsDateTime) => {
  return (
    <span className={`text-1xl font-mono ${className}`}>
      {hour}h {minute}m {second}s
    </span>
  );
};
