
export interface UserProfileTypes {
  userImage?: string;
  username?: string;
};
