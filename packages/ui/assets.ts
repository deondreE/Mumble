// Here there will be all the icons and images used inside the project
export {
  DiApple,
  DiAppstore,
  DiAndroid,
  DiWindows,
  DiAptana
} from 'react-icons/di';

import userDefault from './assets/User-base.svg';
export { userDefault };

export {
  IoMdArrowDropdown
} from 'react-icons/io';

export {
  HiOutlinePlus
} from 'react-icons/hi';

export {
  SiGoogleanalytics
} from 'react-icons/si';
